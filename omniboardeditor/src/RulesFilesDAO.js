var mongoClient = require("mongodb").MongoClient;
var url = "mongodb://localhost:27017/";

class RulesFilesDAO {
    addRulesFile(rulesFile) {

        mongoClient.connect(url, function(err, db) {

    		if (err) throw err;
    		var dbo = db.db("OmniBoardEditor");

    		dbo.collection("rulesFiles").insertOne(rulesFile, function(err, res) {
    			if (err) throw err;
    			else console.log(rulesFile.name + " inserted");
    			db.close();
    		});
    	});
    };

	updateRulesFile(rulesFile) {

        mongoClient.connect(url, function(err, db) {

    		if (err) throw err;
    		var dbo = db.db("OmniBoardEditor");
    		var query = { id: rulesFile.id };
            var changes = {
                $set: {
					name: rulesFile.name,
					flipper: rulesFile.flipper,
					rules: rulesFile.rules
				}
            };

    		dbo.collection("rulesFiles").updateOne(query, changes, function(err, obj) {
    			if (err) throw err;
    			else console.log(rulesFile.name + " updated");
    			db.close();
    		});
    	});
    };

    deleteRulesFile(id) {

        mongoClient.connect(url, function(err, db) {

    		if (err) throw err;
    		var dbo = db.db("OmniBoardEditor");
    		var query = { id: id };

    		dbo.collection("rulesFiles").deleteOne(query, function(err, obj) {
    			if (err) throw err;
    			else console.log(id + " deleted");
    			db.close();
    		});
    	});
    };

    getRulesFiles(callback) {

        mongoClient.connect(url, function(err, db) {

    		if (err) throw err;
    		var dbo = db.db("OmniBoardEditor");
    		var query = {};
            var proj = {};

    		dbo.collection("rulesFiles").find(query, proj).toArray(function(err, result) {
    			if (err) throw err;
    			callback(result);
    			db.close();
    		});
    	});
    };

    getRulesFile(id, callback) {

        mongoClient.connect(url, function(err, db) {

    		if (err) throw err;
    		var dbo = db.db("OmniBoardEditor");
    		var query = {id: id};
            var proj = {};

    		dbo.collection("rulesFiles").find(query, proj).toArray(function(err, result) {
    			if (err) throw err;
    			callback(result);
    			db.close();
    		});
    	});
    };
}

module.exports = RulesFilesDAO;

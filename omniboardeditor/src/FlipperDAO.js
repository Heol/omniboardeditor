var mongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

class FlipperDAO {

    addFlipper(flipper) {

        mongoClient.connect(url, function(err, db) {

    		if (err) throw err;
    		var dbo = db.db("OmniBoardEditor");

    		dbo.collection("flippers").insertOne(flipper, function(err, res) {
    			if (err) throw err;
    			else console.log(flipper.name + " inserted");
    			db.close();
    		});
    	});
    };

    deleteFlipper(name) {

        mongoClient.connect(url, function(err, db) {

    		if (err) throw err;
    		var dbo = db.db("OmniBoardEditor");
    		var aFlipper = { name: name };

    		dbo.collection("flippers").deleteOne(aFlipper, function(err, obj) {
    			if (err) throw err;
    			else console.log(name + " deleted");
    			db.close();
    		});
    	});
    };

    updateFlipper(newName, newLink) {

        mongoClient.connect(url, function(err, db) {

    		if (err) throw err;
    		var dbo = db.db("OmniBoardEditor");
    		var query = { name: newName };
            var changes = {
                $set: {type: newType,
                        name : newName}
            };

    		dbo.collection("flippers").updateOne(query, changes, function(err, obj) {
    			if (err) throw err;
    			else console.log(newName + " updated");
    			db.close();
    		});
    	});
    };

    getFlippers(callback) {

        mongoClient.connect(url, function(err, db) {

            if (err) throw err;
            var dbo = db.db("OmniBoardEditor");
            var query = {};
            var proj = {projection: { _id : 0}};

            dbo.collection("flippers").find(query, proj).toArray(function(err, result) {
                if (err) throw err;
                callback(result);
                db.close();
            });
    	});
    };

    getFlipper(name, callback) {

        mongoClient.connect(url, function(err, db) {

    		if (err) throw err;
    		var dbo = db.db("OmniBoardEditor");
    		var query = {name: name};
            var proj = {projection: { _id : 0, name : 1, link : 1}};

    		dbo.collection("flippers").find(query, proj).toArray(function(err, result) {
    			if (err) throw err;
    			callback(result);
    			db.close();
    		});
    	});
    };
}

module.exports = FlipperDAO;
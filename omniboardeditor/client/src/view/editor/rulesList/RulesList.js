import React from "react";
import { withRouter } from "react-router-dom";

//This class manages the list of rules in a file and save/modify this file based on its savedRules list. It allows to modify the name of the file and displays the pinball name.
class RulesList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: null,
            isLoaded: false,
            savedBlocks: [],
            showPopUp: false,
            name: "",
            id: 0,
            onRestart: props.onRestart,
            onChange: props.onChange,
            currentBlock: props.currentBlock,
            currentFlipper: "",
            currentIndexBlock: 0,
        };
    }

    //Runs when the component is initialize and asks in the database the file informations based on it id in the webapp URL.
    componentDidMount() {
        const fileID = this.props.match.params.id;
        this.setState({ isLoaded: false, id: fileID }, () => {
            let init = { method: "GET" };
            fetch("/file/getfile?id=" + fileID, init)
                .then((res) => {
                    if (res.status >= 400) {
                        throw new Error("Loading Error");
                    }
                    return res.json();
                })
                .then(
                    (file) => {
                        console.log("the file", file);
                        if (file.length === 0) {
                            console.log("This file does not exist in datbase");
                            this.setState({
                                isLoaded: true,
                            });
                        } else {
                            this.setState(
                                {
                                    currentFlipper: file[0].flipper,
                                    name: file[0].name,
                                    isLoaded: true,
                                    savedBlocks: file[0].rules,
                                },
                                () => {
                                    let index = this.state.currentIndexBlock;
                                    let listBlocks = Array.from(
                                        this.state.savedBlocks
                                    );
                                    listBlocks.forEach((block) => {
                                        block.idBlock = index;
                                        index++;
                                    });
                                    this.setState({
                                        savedBlocks: listBlocks,
                                        currentIndexBlock: index,
                                    });
                                }
                            );
                        }
                    },
                    (err) => {
                        this.setState({
                            isLoaded: true,
                            err,
                        });
                    }
                );
        });
    }

    //Changes the local props attributes when it received new props.
    componentWillReceiveProps(newProps) {
        this.setState({
            currentBlock: newProps.currentBlock,
        });
    }

    //Changes the file name when it text field is modified.
    handleFileNameChange = (event) => {
        this.setState({ name: event.target.value });
    };

    //Search a rule in the rules list based on the name in parameter and displays it on the right side of the screen (LogicalBlockComponent)
    loadBlock = (id) => {
        let notFound = true;
        let selectedBlock = {};

        for (let i = 0; i < this.state.savedBlocks.length && notFound; i++) {
            if (this.state.savedBlocks[i].idBlock === id) {
                selectedBlock = this.state.savedBlocks[i];
                notFound = false;
            }
        }

        let counterConditions = 0;
        let counterActions = 0;
        if (selectedBlock.conditions === undefined) {
            counterConditions = undefined;
        } else if (selectedBlock.actions === undefined) {
            counterActions = undefined;
        }

        let ret = {};
        if (!notFound) {
            ret = {
                idBlock: selectedBlock.idBlock,
                type: selectedBlock.type,
                name: selectedBlock.name,
                conditionsCounter: counterConditions,
                conditions: selectedBlock.conditions,
                actionsCounter: counterActions,
                actions: selectedBlock.actions,
            };
        }
        this.state.onChange(ret);
    };

    //Runs a function in it props that restart the displays on the right side of the screen (LogicalBlockComponent) based on it's type (Logical/Action/Conditionnal)
    newRule = (type) => {
        let index = this.state.currentIndexBlock;
        this.state.onRestart(type, index);
        index++;
        this.setState({ currentIndexBlock: index });
    };

    //Saves or modifies the file depends on the changes on the rule list
    saveOrModifyCurrentBlock = () => {
        let nullBlock = false;
        let newName = true;
        let newBlock = {};

        if (this.state.currentBlock.type === "logical_block") {
            newBlock = {
                idBlock: this.state.currentBlock.idBlock,
                type: this.state.currentBlock.type,
                name: this.state.currentBlock.name,
                conditions: this.state.currentBlock.conditions,
                actions: this.state.currentBlock.actions,
            };
        } else if (this.state.currentBlock.type === "conditional_block") {
            newBlock = {
                idBlock: this.state.currentBlock.idBlock,
                type: this.state.currentBlock.type,
                name: this.state.currentBlock.name,
                conditions: this.state.currentBlock.conditions,
            };
        } else if (this.state.currentBlock.type === "action_block") {
            newBlock = {
                idBlock: this.state.currentBlock.idBlock,
                type: this.state.currentBlock.type,
                name: this.state.currentBlock.name,
                actions: this.state.currentBlock.actions,
            };
        } else {
            nullBlock = true;
        }

        if (!nullBlock) {
            let indexToModify = 0;

            for (let i = 0; i < this.state.savedBlocks.length && newName; i++) {
                if (this.state.savedBlocks[i].idBlock === newBlock.idBlock) {
                    newName = false;
                    indexToModify = i;
                }
            }

            let listBlocks = Array.from(this.state.savedBlocks);
            if (newName) {
                listBlocks.push(newBlock);
            } else {
                listBlocks[indexToModify] = newBlock;
            }
            this.setState({ savedBlocks: listBlocks }, () => this.saveRule());
        } else {
            console.log("Le block selectionné est vide");
        }
    };

    //Saves the file in the database.
    saveRule = () => {
        let data = {
            file: {
                id: this.state.id,
                flipper: this.state.currentFlipper,
                name: this.state.name,
                rules: this.state.savedBlocks,
            },
        };

        let init = {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
        };

        fetch("/file/save", init)
            .then((res) => {
                if (res.status >= 400) {
                    throw new Error("Erreur serveur");
                } else if (res === "No file in post request") {
                    console.log("Error: no file in post request");
                } else {
                    console.log("Successful save");
                }
            })
            .then((err) => {
                this.setState({ err });
            });
    };

    //Deletes a rule in the rules list based on it id
    deleteBlock = (id) => {
        const blocks = this.state.savedBlocks.slice();
        const index = blocks.findIndex((block) => block.idBlock === id);
        if (index === this.state.currentBlock.idBlock) {
            this.newRule("");
        }
        blocks.splice(index, 1);
        this.setState({ savedBlocks: blocks }, () => {
            this.saveRule();
        });
    };

    //Displays three four buttons: Save, New Action Block, New Logical Block and New Conditionnal Block.
    //And then, if they are no errors, the file's rules list. Each rule is displays with a delete button next it to use when you want to delete the rule.
    //If an error appear during the loading of the file rules list. It displays this error instead.
    render() {
        const { err, isLoaded, savedBlocks } = this.state;
        let listRules;
        if (err) {
            listRules = <div>Loading error: {err.message}</div>;
        } else if (!isLoaded) {
            listRules = <div>Loading...</div>;
        } else {
            listRules = (
                <div>
                    {savedBlocks.map((block) => (
                        <div key={block.idBlock}>
                            <button
                                className="button-block"
                                onClick={() => this.loadBlock(block.idBlock)}
                            >
                                <div>
                                    <div id="blockname">{block.name}</div>
                                    <br />
                                    <div id="blocktype">
                                        <i>{block.type}</i>
                                    </div>
                                </div>
                            </button>
                            <button
                                className="delete-rule"
                                onClick={() => this.deleteBlock(block.idBlock)}
                            >
                                X
                            </button>
                        </div>
                    ))}
                </div>
            );
        }
        return (
            <div className="ruleslist" id="ruleslist">
                <div>
                    <input
                        className="input-fileName"
                        value={this.state.name}
                        onChange={this.handleFileNameChange}
                        id="filename"
                    />
                    <div className="block-title">
                        On: {this.state.currentFlipper}
                    </div>
                </div>
                <div className="actionButton">
                    <button
                        className="button-save-creationrules"
                        onClick={() => {
                            this.saveOrModifyCurrentBlock();
                        }}
                    >
                        SAVE
                    </button>

                    <center>
                        <h3 className="sub-title-createrules">New blocks :</h3>
                    </center>

                    <button
                        className="button-block"
                        onClick={() => {
                            this.newRule("logical_block");
                        }}
                    >
                        New Logical Block
                    </button>
                    <button
                        className="button-block"
                        onClick={() => {
                            this.newRule("action_block");
                        }}
                    >
                        New Action Block
                    </button>
                    <button
                        className="button-block"
                        onClick={() => {
                            this.newRule("conditional_block");
                        }}
                    >
                        New Condition Block
                    </button>
                </div>

                <center>
                    <h3 className="sub-title-createrules">Rules :</h3>
                </center>
                <div className="button">{listRules}</div>
            </div>
        );
    }
}
//It use withRouter to be able to use the Router component features like taking a variable in the website URL
export default withRouter(RulesList);

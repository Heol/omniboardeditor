import React from "react";

import RulesList from "./rulesList/RulesList.js";
import LogicalBlockComponent from "./blocks/LogicalBlockComponent.js";
import { Link } from "react-router-dom";

// Main component of the editor's page. Contains one RulesList on the left, and
// a LogicalBlockComponent on the right
class EditorComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            openedRule: {
                idBlock: 0,
                type: "",
                name: "",
                conditionsCounter: 0,
                conditions: [],
                actionsCounter: 0,
                actions: [],
            },
        };
    }

    // Lift the change from the logical block or the rule list to the whole editor
    onLogicalBlockChange = (newBlock) => {
        this.setState({
            openedRule: {
                idBlock: newBlock.idBlock,
                type: newBlock.type,
                name: newBlock.name,
                conditionsCounter: newBlock.conditionsCounter,
                conditions: newBlock.conditions,
                actionsCounter: newBlock.actionsCounter,
                actions: newBlock.actions,
            },
        });
    };

    // Reset the currently opened rule.
    // Used when creating new rule from the rules list
    onLogicalBlockRestart = (newType, newIndex) => {
        this.setState({
            openedRule: {
                idBlock: newIndex,
                type: newType,
                name: "",
                conditionsCounter: 0,
                conditions: [],
                actionsCounter: 0,
                actions: [],
            },
        });
    };

    render() {
        return (
            <div>
                <h1 className="title-menu">Create your rules</h1>
                <div className="editor">
                    <RulesList
                        onChange={this.onLogicalBlockChange}
                        onRestart={this.onLogicalBlockRestart}
                        currentBlock={this.state.openedRule}
                    />

                    <LogicalBlockComponent
                        onChange={this.onLogicalBlockChange}
                        currentBlock={this.state.openedRule}
                    />
                </div>

                <center>
                    <Link to="/">
                        <button className="exit-button">Exit</button>
                    </Link>
                </center>
            </div>
        );
    }
}

export default EditorComponent;

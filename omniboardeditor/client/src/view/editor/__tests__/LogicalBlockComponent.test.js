import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";

import LogicalBlockComponent from "../blocks/LogicalBlockComponent.js";

configure({ adapter: new Adapter() });

describe("[LogicalBlockComponent] Condition creation from LogicalBlock", () => {
    const currentBlock = {
        type: "logical_block",
        name: "",
        conditionsCounter: 0,
        conditions: [],
        actionsCounter: 0,
        actions: [],
    };

    const onChange = (b) => {};

    let component;
    beforeEach(() => {
        component = shallow(
            <LogicalBlockComponent
                onChange={onChange}
                currentBlock={currentBlock}
            />
        ).instance();
    });

    test("Create 1 condition (addCondition method)", () => {
        const cond = {
            name: "test cond",
            input: "test input",
            operation: "test operation",
            value: "test value",
        };

        component.addCondition(cond);

        expect(component.state.conditionsCounter).toEqual(1);

        const stateConditions = component.state.conditions;
        expect(stateConditions.length).toEqual(1);

        const stateCond = stateConditions[0];
        expect(stateCond.id).toEqual(0);
        expect(stateCond.name).toEqual("test cond");
        expect(stateCond.input).toEqual("test input");
        expect(stateCond.operation).toEqual("test operation");
        expect(stateCond.value).toEqual("test value");
    });

    test("Create 1 empty condition (addEmptyCondition method)", () => {
        component.addEmptyCondition();

        expect(component.state.conditionsCounter).toEqual(1);

        const stateConditions = component.state.conditions;
        expect(stateConditions.length).toEqual(1);

        const stateCond = stateConditions[0];
        expect(stateCond.id).toEqual(0);
        expect(stateCond.name).toEqual("");
        expect(stateCond.input).toEqual("");
        expect(stateCond.operation).toEqual("");
        expect(stateCond.value).toEqual("");
    });

    test("Create 3 empty condition (addEmptyCondition method) => increments counter", () => {
        component.addEmptyCondition();
        component.addEmptyCondition();
        component.addEmptyCondition();

        expect(component.state.conditionsCounter).toEqual(3);

        const stateConditions = component.state.conditions;
        expect(stateConditions.length).toEqual(3);

        const stateCond1 = stateConditions[0];
        expect(stateCond1.id).toEqual(0);
        expect(stateCond1.name).toEqual("");
        expect(stateCond1.input).toEqual("");
        expect(stateCond1.operation).toEqual("");
        expect(stateCond1.value).toEqual("");

        const stateCond2 = stateConditions[1];
        expect(stateCond2.id).toEqual(1);
        expect(stateCond2.name).toEqual("");
        expect(stateCond2.input).toEqual("");
        expect(stateCond2.operation).toEqual("");
        expect(stateCond2.value).toEqual("");

        const stateCond3 = stateConditions[2];
        expect(stateCond3.id).toEqual(2);
        expect(stateCond3.name).toEqual("");
        expect(stateCond3.input).toEqual("");
        expect(stateCond3.operation).toEqual("");
        expect(stateCond3.value).toEqual("");
    });
});

describe("[LogicalBlockComponent] Condition delete from LogicalBlock", () => {
    const currentBlock = {
        type: "logical_block",
        name: "",
        conditionsCounter: 0,
        conditions: [],
        actionsCounter: 0,
        actions: [],
    };

    const onChange = (b) => {};

    let component;
    beforeEach(() => {
        component = shallow(
            <LogicalBlockComponent
                onChange={onChange}
                currentBlock={currentBlock}
            />
        ).instance();
    });

    test("Add 1 empty condition, then delete it. Counter doesn't change", () => {
        // Add condition, its id = 0
        component.addEmptyCondition();

        // Delete condition with id = 0
        component.deleteCondition(0);

        expect(component.state.conditions.length).toEqual(0);
        expect(component.state.conditionsCounter).toEqual(1);
    });
});

describe("[LogicalBlockComponent] Condition change from LogicalBlock", () => {
    const currentBlock = {
        type: "logical_block",
        name: "",
        conditionsCounter: 0,
        conditions: [],
        actionsCounter: 0,
        actions: [],
    };

    const onChange = (b) => {};

    let component;
    beforeEach(() => {
        component = shallow(
            <LogicalBlockComponent
                onChange={onChange}
                currentBlock={currentBlock}
            />
        ).instance();
    });

    test("Add 3 different conditions, then modify one (onConditionChange)", () => {
        const cond1 = {
            name: "test cond 1",
            input: "test input 1",
            operation: "test operation 1",
            value: "test value 1",
        };

        const cond2 = {
            name: "test cond 2",
            input: "test input 2",
            operation: "test operation 2",
            value: "test value 2",
        };

        const cond3 = {
            name: "test cond 3",
            input: "test input 3",
            operation: "test operation 3",
            value: "test value 3",
        };

        component.addCondition(cond1);
        component.addCondition(cond2);
        component.addCondition(cond3);

        const newCond2 = {
            name: "COND 2",
            input: "INPUT 2",
            operation: "OPERATION 2",
            value: "VALUE 2",
        };

        // triggerChange is a function to modify the condition with id = 1 (here the second)
        const triggerChange = component.onConditionChange(1);
        triggerChange(newCond2);

        const newConds = [
            {
                id: 0,
                name: "test cond 1",
                input: "test input 1",
                operation: "test operation 1",
                value: "test value 1",
            },
            {
                id: 1,
                name: "COND 2",
                input: "INPUT 2",
                operation: "OPERATION 2",
                value: "VALUE 2",
            },
            {
                id: 2,
                name: "test cond 3",
                input: "test input 3",
                operation: "test operation 3",
                value: "test value 3",
            },
        ];

        expect(component.state.conditionsCounter).toEqual(3);
        expect(component.state.conditions).toEqual(newConds);
    });
});

describe("[LogicalBlockComponent] Action creation from LogicalBlock", () => {
    const currentBlock = {
        type: "logical_block",
        name: "",
        conditionsCounter: 0,
        conditions: [],
        actionsCounter: 0,
        actions: [],
    };

    const onChange = (b) => {};

    let component;
    beforeEach(() => {
        component = shallow(
            <LogicalBlockComponent
                onChange={onChange}
                currentBlock={currentBlock}
            />
        ).instance();
    });

    test("Create 1 action (addAction method)", () => {
        const act = {
            name: "test act",
            action: "test action",
            output: "test output",
            value: "test value",
            param: "test param",
        };

        component.addAction(act);

        expect(component.state.actionsCounter).toEqual(1);

        const stateActions = component.state.actions;
        expect(stateActions.length).toEqual(1);

        const stateAct = stateActions[0];
        expect(stateAct.id).toEqual(0);
        expect(stateAct.name).toEqual("test act");
        expect(stateAct.action).toEqual("test action");
        expect(stateAct.output).toEqual("test output");
        expect(stateAct.value).toEqual("test value");
        expect(stateAct.param).toEqual("test param");
    });

    test("Create 1 empty action (addEmptyAction method)", () => {
        component.addEmptyAction();

        expect(component.state.actionsCounter).toEqual(1);

        const stateActions = component.state.actions;
        expect(stateActions.length).toEqual(1);

        const stateAct = stateActions[0];
        expect(stateAct.id).toEqual(0);
        expect(stateAct.name).toEqual("");
        expect(stateAct.action).toEqual("");
        expect(stateAct.output).toEqual("");
        expect(stateAct.value).toEqual("");
        expect(stateAct.param).toEqual("");
    });

    test("Create 3 empty action (addEmptyAction method) => increments counter", () => {
        component.addEmptyAction();
        component.addEmptyAction();
        component.addEmptyAction();

        expect(component.state.actionsCounter).toEqual(3);

        const stateActions = component.state.actions;
        expect(stateActions.length).toEqual(3);

        const stateAct1 = stateActions[0];
        expect(stateAct1.id).toEqual(0);
        expect(stateAct1.name).toEqual("");
        expect(stateAct1.action).toEqual("");
        expect(stateAct1.output).toEqual("");
        expect(stateAct1.value).toEqual("");
        expect(stateAct1.param).toEqual("");

        const stateAct2 = stateActions[1];
        expect(stateAct2.id).toEqual(1);
        expect(stateAct2.name).toEqual("");
        expect(stateAct2.action).toEqual("");
        expect(stateAct2.output).toEqual("");
        expect(stateAct2.value).toEqual("");
        expect(stateAct2.param).toEqual("");

        const stateAct3 = stateActions[2];
        expect(stateAct3.id).toEqual(2);
        expect(stateAct3.name).toEqual("");
        expect(stateAct3.action).toEqual("");
        expect(stateAct3.output).toEqual("");
        expect(stateAct3.value).toEqual("");
        expect(stateAct3.param).toEqual("");
    });
});

describe("[LogicalBlockComponent] Action delete from LogicalBlock", () => {
    const currentBlock = {
        type: "logical_block",
        name: "",
        conditionsCounter: 0,
        conditions: [],
        actionsCounter: 0,
        actions: [],
    };

    const onChange = (b) => {};

    let component;
    beforeEach(() => {
        component = shallow(
            <LogicalBlockComponent
                onChange={onChange}
                currentBlock={currentBlock}
            />
        ).instance();
    });

    test("Add 1 empty action, then delete it. Counter doesn't change", () => {
        // Add action, its id = 0
        component.addEmptyAction();

        // Delete action with id = 0
        component.deleteAction(0);

        expect(component.state.actions.length).toEqual(0);
        expect(component.state.actionsCounter).toEqual(1);
    });
});

describe("[LogicalBlockComponent] Action change from LogicalBlock", () => {
    const currentBlock = {
        type: "logical_block",
        name: "",
        conditionsCounter: 0,
        conditions: [],
        actionsCounter: 0,
        actions: [],
    };

    const onChange = (b) => {};

    let component;
    beforeEach(() => {
        component = shallow(
            <LogicalBlockComponent
                onChange={onChange}
                currentBlock={currentBlock}
            />
        ).instance();
    });

    test("Add 3 different actions, then modify one (onActionChange)", () => {
        const act1 = {
            name: "test act 1",
            action: "test action 1",
            output: "test output 1",
            value: "test value 1",
            param: "test param 1",
        };

        const act2 = {
            name: "test act 2",
            action: "test action 2",
            output: "test output 2",
            value: "test value 2",
            param: "test param 2",
        };

        const act3 = {
            name: "test act 3",
            action: "test action 3",
            output: "test output 3",
            value: "test value 3",
            param: "test param 3",
        };

        component.addAction(act1);
        component.addAction(act2);
        component.addAction(act3);

        const newAct2 = {
            name: "ACT 2",
            action: "ACTION 2",
            output: "OUTPUT 2",
            value: "VALUE 2",
            param: "PARAM 2",
        };

        // triggerChange is a function to modify the action with id = 1 (here the seact)
        const triggerChange = component.onActionChange(1);
        triggerChange(newAct2);

        const newActs = [
            {
                id: 0,
                name: "test act 1",
                action: "test action 1",
                output: "test output 1",
                value: "test value 1",
                param: "test param 1",
            },
            {
                id: 1,
                name: "ACT 2",
                action: "ACTION 2",
                output: "OUTPUT 2",
                value: "VALUE 2",
                param: "PARAM 2",
            },
            {
                id: 2,
                name: "test act 3",
                action: "test action 3",
                output: "test output 3",
                value: "test value 3",
                param: "test param 3",
            },
        ];

        expect(component.state.actionsCounter).toEqual(3);
        expect(component.state.actions).toEqual(newActs);
    });
});

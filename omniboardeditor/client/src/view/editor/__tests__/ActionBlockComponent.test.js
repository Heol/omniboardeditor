import { fireEvent, render, screen } from "@testing-library/react";

import ActionBlockComponent from "../blocks/ActionBlockComponent.js";

describe("[ActionBlockComponent] Change ActionBlockComponent name using the input", () => {
    const changeFunc = (id) => {};
    const deleteFunc = (id) => {};

    let wrapper;

    beforeEach(() => {
        wrapper = render(
            <ActionBlockComponent
                name=""
                action=""
                output=""
                value=""
                param=""
                changeAction={changeFunc}
                deleteAction={deleteFunc}
            />
        );
    });

    test(`Name: "test name"`, () => {
        let nameInput = screen.getByLabelText(/name/i);

        fireEvent.change(nameInput, { target: { value: "test name" } });
        expect(nameInput.value).toEqual("test name");
    });

    test(`Name: "test_name"`, () => {
        let nameInput = screen.getByLabelText(/name/i);

        fireEvent.change(nameInput, { target: { value: "test name" } });
        expect(nameInput.value).toEqual("test name");
    });

    test(`Name: "test-name"`, () => {
        let nameInput = screen.getByLabelText(/name/i);

        fireEvent.change(nameInput, { target: { value: "test name" } });
        expect(nameInput.value).toEqual("test name");
    });

    test(`Name: ""`, () => {
        let nameInput = screen.getByLabelText(/name/i);

        fireEvent.change(nameInput, { target: { value: "" } });
        expect(nameInput.value).toEqual("");
    });
});

describe("[ActionBlockComponent] Change ActionBlockComponent action using the input", () => {
    const changeFunc = (id) => {};
    const deleteFunc = (id) => {};

    let wrapper;

    beforeEach(() => {
        wrapper = render(
            <ActionBlockComponent
                name=""
                action=""
                output=""
                value=""
                param=""
                changeAction={changeFunc}
                deleteAction={deleteFunc}
            />
        );
    });

    test(`Action: "test action"`, () => {
        let actionInput = screen.getByLabelText(/action/i);

        fireEvent.change(actionInput, { target: { value: "test action" } });
        expect(actionInput.value).toEqual("test action");
    });

    test(`Action: "test_action"`, () => {
        let actionInput = screen.getByLabelText(/action/i);

        fireEvent.change(actionInput, { target: { value: "test_action" } });
        expect(actionInput.value).toEqual("test_action");
    });

    test(`Action: "test-action"`, () => {
        let actionInput = screen.getByLabelText(/action/i);

        fireEvent.change(actionInput, { target: { value: "test-action" } });
        expect(actionInput.value).toEqual("test-action");
    });

    test(`Action: ""`, () => {
        let actionInput = screen.getByLabelText(/action/i);

        fireEvent.change(actionInput, { target: { value: "" } });
        expect(actionInput.value).toEqual("");
    });
});

describe("[ActionBlockComponent] Change ActionBlockComponent output using the input", () => {
    const changeFunc = (id) => {};
    const deleteFunc = (id) => {};

    let wrapper;

    beforeEach(() => {
        wrapper = render(
            <ActionBlockComponent
                name=""
                action=""
                output=""
                value=""
                param=""
                changeAction={changeFunc}
                deleteAction={deleteFunc}
            />
        );
    });

    test(`Output: "test output"`, () => {
        let outputInput = screen.getByLabelText(/output/i);

        fireEvent.change(outputInput, { target: { value: "test output" } });
        expect(outputInput.value).toEqual("test output");
    });

    test(`Output: "test_output"`, () => {
        let outputInput = screen.getByLabelText(/output/i);

        fireEvent.change(outputInput, { target: { value: "test_output" } });
        expect(outputInput.value).toEqual("test_output");
    });

    test(`Output: "test-output"`, () => {
        let outputInput = screen.getByLabelText(/output/i);

        fireEvent.change(outputInput, { target: { value: "test-output" } });
        expect(outputInput.value).toEqual("test-output");
    });

    test(`Output: ""`, () => {
        let outputInput = screen.getByLabelText(/output/i);

        fireEvent.change(outputInput, { target: { value: "" } });
        expect(outputInput.value).toEqual("");
    });
});

describe("[ActionBlockComponent] Change ActionBlockComponent value using the input", () => {
    const changeFunc = (id) => {};
    const deleteFunc = (id) => {};

    let wrapper;

    beforeEach(() => {
        wrapper = render(
            <ActionBlockComponent
                name=""
                action=""
                output=""
                value=""
                param=""
                changeAction={changeFunc}
                deleteAction={deleteFunc}
            />
        );
    });

    test(`Value: "test value"`, () => {
        let valueInput = screen.getByLabelText(/value/i);

        fireEvent.change(valueInput, {
            target: { value: "test value" },
        });
        expect(valueInput.value).toEqual("test value");
    });

    test(`Value: "test_value"`, () => {
        let valueInput = screen.getByLabelText(/value/i);

        fireEvent.change(valueInput, {
            target: { value: "test_value" },
        });
        expect(valueInput.value).toEqual("test_value");
    });

    test(`Value: "test-value"`, () => {
        let valueInput = screen.getByLabelText(/value/i);

        fireEvent.change(valueInput, {
            target: { value: "test-value" },
        });
        expect(valueInput.value).toEqual("test-value");
    });

    test(`Value: ""`, () => {
        let valueInput = screen.getByLabelText(/value/i);

        fireEvent.change(valueInput, { target: { value: "" } });
        expect(valueInput.value).toEqual("");
    });
});

describe("[ActionBlockComponent] Change ActionBlockComponent parameter using the input", () => {
    const changeFunc = (id) => {};
    const deleteFunc = (id) => {};

    let wrapper;

    beforeEach(() => {
        wrapper = render(
            <ActionBlockComponent
                name=""
                action=""
                output=""
                value=""
                param=""
                changeAction={changeFunc}
                deleteAction={deleteFunc}
            />
        );
    });

    test(`Parameter: "test param"`, () => {
        let paramInput = screen.getByLabelText(/param/i);

        fireEvent.change(paramInput, {
            target: { value: "test param" },
        });
        expect(paramInput.value).toEqual("test param");
    });

    test(`Parameter: "test_param"`, () => {
        let paramInput = screen.getByLabelText(/param/i);

        fireEvent.change(paramInput, {
            target: { value: "test_param" },
        });
        expect(paramInput.value).toEqual("test_param");
    });

    test(`Parameter: "test-param"`, () => {
        let paramInput = screen.getByLabelText(/param/i);

        fireEvent.change(paramInput, {
            target: { value: "test-param" },
        });
        expect(paramInput.value).toEqual("test-param");
    });

    test(`Parameter: ""`, () => {
        let paramInput = screen.getByLabelText(/param/i);

        fireEvent.change(paramInput, { target: { value: "" } });
        expect(paramInput.value).toEqual("");
    });
});

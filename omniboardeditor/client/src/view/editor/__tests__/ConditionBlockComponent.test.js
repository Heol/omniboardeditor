import { fireEvent, render, screen } from "@testing-library/react";

import ConditionBlockComponent from "../blocks/ConditionBlockComponent.js";

describe("[ConditionBlockComponent] Change ConditionBlockComponent name using the input", () => {
    const changeFunc = (id) => {};
    const deleteFunc = (id) => {};

    let wrapper;

    beforeEach(() => {
        wrapper = render(
            <ConditionBlockComponent
                name=""
                input=""
                operation=""
                value=""
                changeAction={changeFunc}
                deleteAction={deleteFunc}
            />
        );
    });

    test(`Name: "test name"`, () => {
        let nameInput = screen.getByLabelText(/name/i);

        fireEvent.change(nameInput, { target: { value: "test name" } });
        expect(nameInput.value).toEqual("test name");
    });

    test(`Name: "test_name"`, () => {
        let nameInput = screen.getByLabelText(/name/i);

        fireEvent.change(nameInput, { target: { value: "test_name" } });
        expect(nameInput.value).toEqual("test_name");
    });

    test(`Name: "test-name"`, () => {
        let nameInput = screen.getByLabelText(/name/i);

        fireEvent.change(nameInput, { target: { value: "test-name" } });
        expect(nameInput.value).toEqual("test-name");
    });

    test(`Name: ""`, () => {
        let nameInput = screen.getByLabelText(/name/i);

        fireEvent.change(nameInput, { target: { value: "" } });
        expect(nameInput.value).toEqual("");
    });
});

describe("[ConditionBlockComponent] Change ConditionBlockComponent input using the input", () => {
    const changeFunc = (id) => {};
    const deleteFunc = (id) => {};

    let wrapper;

    beforeEach(() => {
        wrapper = render(
            <ConditionBlockComponent
                name=""
                input=""
                operation=""
                value=""
                changeAction={changeFunc}
                deleteAction={deleteFunc}
            />
        );
    });

    test(`Input: "test input"`, () => {
        let inputInput = screen.getByLabelText(/input/i);

        fireEvent.change(inputInput, { target: { value: "test input" } });
        expect(inputInput.value).toEqual("test input");
    });

    test(`Input: "test_input"`, () => {
        let inputInput = screen.getByLabelText(/input/i);

        fireEvent.change(inputInput, { target: { value: "test_input" } });
        expect(inputInput.value).toEqual("test_input");
    });

    test(`Input: "test-input"`, () => {
        let inputInput = screen.getByLabelText(/input/i);

        fireEvent.change(inputInput, { target: { value: "test-input" } });
        expect(inputInput.value).toEqual("test-input");
    });

    test(`Input: ""`, () => {
        let inputInput = screen.getByLabelText(/input/i);

        fireEvent.change(inputInput, { target: { value: "" } });
        expect(inputInput.value).toEqual("");
    });
});

describe("[ConditionBlockComponent] Change ConditionBlockComponent operation using the input", () => {
    const changeFunc = (id) => {};
    const deleteFunc = (id) => {};

    let wrapper;

    beforeEach(() => {
        wrapper = render(
            <ConditionBlockComponent
                name=""
                input=""
                operation=""
                value=""
                changeAction={changeFunc}
                deleteAction={deleteFunc}
            />
        );
    });

    test(`Operation: "test operation"`, () => {
        let operationInput = screen.getByLabelText(/operation/i);

        fireEvent.change(operationInput, {
            target: { value: "test operation" },
        });
        expect(operationInput.value).toEqual("test operation");
    });

    test(`Operation: "test_operation"`, () => {
        let operationInput = screen.getByLabelText(/operation/i);

        fireEvent.change(operationInput, {
            target: { value: "test_operation" },
        });
        expect(operationInput.value).toEqual("test_operation");
    });

    test(`Operation: "test-operation"`, () => {
        let operationInput = screen.getByLabelText(/operation/i);

        fireEvent.change(operationInput, {
            target: { value: "test-operation" },
        });
        expect(operationInput.value).toEqual("test-operation");
    });

    test(`Operation: ""`, () => {
        let operationInput = screen.getByLabelText(/operation/i);

        fireEvent.change(operationInput, { target: { value: "" } });
        expect(operationInput.value).toEqual("");
    });
});

describe("[ConditionBlockComponent] Change ConditionBlockComponent value using the input", () => {
    const changeFunc = (id) => {};
    const deleteFunc = (id) => {};

    let wrapper;

    beforeEach(() => {
        wrapper = render(
            <ConditionBlockComponent
                name=""
                input=""
                operation=""
                value=""
                changeAction={changeFunc}
                deleteAction={deleteFunc}
            />
        );
    });

    test(`Value: "test value"`, () => {
        let valueInput = screen.getByLabelText(/value/i);

        fireEvent.change(valueInput, {
            target: { value: "test value" },
        });
        expect(valueInput.value).toEqual("test value");
    });

    test(`Value: "test_value"`, () => {
        let valueInput = screen.getByLabelText(/value/i);

        fireEvent.change(valueInput, {
            target: { value: "test_value" },
        });
        expect(valueInput.value).toEqual("test_value");
    });

    test(`Value: "test-value"`, () => {
        let valueInput = screen.getByLabelText(/value/i);

        fireEvent.change(valueInput, {
            target: { value: "test-value" },
        });
        expect(valueInput.value).toEqual("test-value");
    });

    test(`Value: ""`, () => {
        let valueInput = screen.getByLabelText(/value/i);

        fireEvent.change(valueInput, { target: { value: "" } });
        expect(valueInput.value).toEqual("");
    });
});

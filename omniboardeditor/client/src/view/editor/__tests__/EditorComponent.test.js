import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";

import EditorComponent from "../EditorComponent.js";

configure({ adapter: new Adapter() });

describe("[EditorComponent] Change the current logical block (onLogicalBlockChange method)", () => {
    let component;
    beforeEach(() => {
        component = shallow(<EditorComponent />).instance();
    });

    test("Change the current block", () => {
        const newBlock = {
            name: "block test",
            type: "logical_block",
            conditionsCounter: 1,
            conditions: [
                {
                    name: "new cond test 1",
                    input: "new input 1",
                    operation: "new operation 1",
                    value: "new value 1",
                },
            ],
            actionsCounter: 1,
            actions: [],
        };

        component.onLogicalBlockChange(newBlock);

        const newState = {
            openedRule: {
                name: "block test",
                type: "logical_block",
                conditionsCounter: 1,
                conditions: [
                    {
                        name: "new cond test 1",
                        input: "new input 1",
                        operation: "new operation 1",
                        value: "new value 1",
                    },
                ],
                actionsCounter: 1,
                actions: [],
            },
        };

        expect(component.state).toEqual(newState);
    });
});

describe("[EditorComponent] Change the type of the current logical block (onLogicalBlockRestart method)", () => {
    let component;
    beforeEach(() => {
        component = shallow(<EditorComponent />).instance();
    });

    test(`Change the type to "action_block"`, () => {
        component.onLogicalBlockRestart("action_block");

        const newState = {
            openedRule: {
                type: "action_block",
                name: "",
                conditionsCounter: 0,
                conditions: [],
                actionsCounter: 0,
                actions: [],
            },
        };

        expect(component.state).toEqual(newState);
    });
});

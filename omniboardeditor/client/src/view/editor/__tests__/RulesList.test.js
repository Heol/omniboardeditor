import React from "react";
import { configure } from "enzyme";
import { Router, Route } from "react-router-dom";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { createMemoryHistory } from "history";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";

import RulesList from "../rulesList/RulesList.js";

const onChange = (newBlock) => {};
const onRestart = (newType) => {};

configure({ adapter: new Adapter() });

function renderWithRouterMatch(
    ui,
    {
        path = "/",
        route = "/",
        history = createMemoryHistory({ initialEntries: [route] }),
        props = {
            currentBlock: {
                type: "",
                name: "",
                conditionsCounter: 0,
                conditions: [],
                actionsCounter: 0,
                actions: [],
            },
            onChange: { onChange },
            onRestart: { onRestart },
        },
    } = {}
) {
    return {
        ...render(
            <Router history={history}>
                <Route path={path} component={ui} {...props}></Route>
            </Router>
        ),
    };
}

describe("RulesList Test", () => {
    describe("Render Test", () => {
        let getByText;

        beforeEach(() => {
            getByText = renderWithRouterMatch(RulesList, {
                route: "/editor/125456",
                path: "/editor/:id",
            }).getByText;
        });

        it("Tests the Save button to be in the render", () => {
            expect(getByText("SAVE")).toBeInTheDocument();
        });

        it("Tests the New Block text to be in the render", () => {
            expect(getByText("New blocks :")).toBeInTheDocument();
        });

        it("Tests the New Logical Block button to be in the render", () => {
            expect(getByText("New Logical Block")).toBeInTheDocument();
        });

        it("Tests the New Condition Block button to be in the render", () => {
            expect(getByText("New Condition Block")).toBeInTheDocument();
        });

        it("Tests the New Action Block button to be in the render", () => {
            expect(getByText("New Action Block")).toBeInTheDocument();
        });

        it("Tests the Rules text to be in the render", () => {
            expect(getByText("Rules :")).toBeInTheDocument();
        });
    });
});

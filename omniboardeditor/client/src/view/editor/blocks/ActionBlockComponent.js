import React from "react";

/*
 * Represents an action element, with 5 inputs
 */
class ActionBlockComponent extends React.Component {
    constructor(props) {
        super(props);

        // Utiliser classe de Donovan ici (Condition.js)
        this.state = {
            name: props.name,
            action: props.action,
            output: props.output,
            value: props.value,
            param: props.param,

            deleteAction: props.deleteAction,
            changeAction: props.changeAction,
        };
    }

    // Change "name" value in state according to the input. And lifts the value to LogicalBlock's state
    handleNameChange = (event) => {
        // Compute the property name dynamically
        this.setState(
            {
                name: event.target.value,
            },
            () => {
                // Change logical block state
                const newAct = {
                    name: this.state.name,
                    action: this.state.action,
                    output: this.state.output,
                    value: this.state.value,
                    param: this.state.param,
                };

                this.state.changeAction(newAct);
            }
        );
    };

    // Change "action" value in state according to the input. And lifts the value to LogicalBlock's state
    handleActionChange = (event) => {
        // Compute the property name dynamically
        this.setState(
            {
                action: event.target.value,
            },
            () => {
                // Change logical block state
                const newAct = {
                    name: this.state.name,
                    action: this.state.action,
                    output: this.state.output,
                    value: this.state.value,
                    param: this.state.param,
                };

                this.state.changeAction(newAct);
            }
        );
    };

    // Change "output" value in state according to the input. And lifts the value to LogicalBlock's state
    handleOutputChange = (event) => {
        // Compute the property name dynamically
        this.setState(
            {
                output: event.target.value,
            },
            () => {
                // Change logical block state
                const newAct = {
                    name: this.state.name,
                    action: this.state.action,
                    output: this.state.output,
                    value: this.state.value,
                    param: this.state.param,
                };

                this.state.changeAction(newAct);
            }
        );
    };

    // Change "value" value in state according to the input. And lifts the value to LogicalBlock's state
    handleValueChange = (event) => {
        // Compute the property name dynamically
        this.setState(
            {
                value: event.target.value,
            },
            () => {
                // Change logical block state
                const newAct = {
                    name: this.state.name,
                    action: this.state.action,
                    output: this.state.output,
                    value: this.state.value,
                    param: this.state.param,
                };

                this.state.changeAction(newAct);
            }
        );
    };

    // Change "parameter" value in state according to the input. And lifts the value to LogicalBlock's state
    handleParamChange = (event) => {
        // Compute the property name dynamically
        this.setState(
            {
                param: event.target.value,
            },
            () => {
                // Change logical block state
                const newAct = {
                    name: this.state.name,
                    action: this.state.action,
                    output: this.state.output,
                    value: this.state.value,
                    param: this.state.param,
                };

                this.state.changeAction(newAct);
            }
        );
    };

    render() {
        return (
            <center>
                <div className="block">
                    <form>
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        <label className="block-label" htmlFor="ab_name">Name</label>
                                    </td>
                                    <td>
                                        <input
                                            className="block-input"
                                            type="text"
                                            id="ab_name"
                                            name="ab_name"
                                            value={this.state.name}
                                            onChange={this.handleNameChange}
                                        />
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <label htmlFor="ab_action" className="block-label">
                                            Action
                                        </label>
                                    </td>
                                    <td>
                                        <input
                                            className="block-input"
                                            type="text"
                                            id="ab_action"
                                            name="ab_action"
                                            value={this.state.action}
                                            onChange={this.handleActionChange}
                                        />
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <label htmlFor="ab_output"className="block-label" >
                                            Output
                                        </label>
                                    </td>
                                    <td>
                                        <input
                                            className="block-input"
                                            type="text"
                                            id="ab_output"
                                            name="ab_output"
                                            value={this.state.output}
                                            onChange={this.handleOutputChange}
                                        />
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <label htmlFor="ab_value"className="block-label" >Value</label>
                                    </td>
                                    <td>
                                        <input
                                            className="block-input"
                                            type="text"
                                            id="ab_value"
                                            name="ab_value"
                                            value={this.state.value}
                                            onChange={this.handleValueChange}
                                        />
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <label htmlFor="ab_param" className="block-label">
                                            Parameter
                                        </label>
                                    </td>
                                    <td>
                                        <input
                                            className="block-input"
                                            type="text"
                                            id="ab_param"
                                            name="ab_param"
                                            value={this.state.param}
                                            onChange={this.handleParamChange}
                                        />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                    <button className="block-button" onClick={this.state.deleteAction}>Delete</button>
                </div>
            </center>
        );
    }
}

export default ActionBlockComponent;

import React from "react";

/*
 * Represents a condition element, with 4 inputs
 */
class ConditionBlockComponent extends React.Component {
    constructor(props) {
        super(props);

        // Utiliser classe de Donovan ici (Condition.js)
        this.state = {
            name: props.name,
            input: props.input,
            operation: props.operation,
            value: props.value,

            deleteAction: props.deleteAction,
            changeAction: props.changeAction,
        };
    }

    // Change "name" value in state according to the input. And lifts the value to LogicalBlock's state
    handleNameChange = (event) => {
        this.setState(
            {
                name: event.target.value,
            },
            () => {
                // Change logical block state
                const newCond = {
                    name: this.state.name,
                    input: this.state.input,
                    operation: this.state.operation,
                    value: this.state.value,
                };

                this.state.changeAction(newCond);
            }
        );
    };

    // Change "input" value in state according to the input. And lifts the value to LogicalBlock's state
    handleInputChange = (event) => {
        this.setState(
            {
                input: event.target.value,
            },
            () => {
                // Change logical block state
                const newCond = {
                    name: this.state.name,
                    input: this.state.input,
                    operation: this.state.operation,
                    value: this.state.value,
                };

                this.state.changeAction(newCond);
            }
        );
    };

    // Change "operation" value in state according to the input. And lifts the value to LogicalBlock's state
    handleOperationChange = (event) => {
        this.setState(
            {
                operation: event.target.value,
            },
            () => {
                // Change logical block state
                const newCond = {
                    name: this.state.name,
                    input: this.state.input,
                    operation: this.state.operation,
                    value: this.state.value,
                };

                this.state.changeAction(newCond);
            }
        );
    };

    // Change "value" value in state according to the input. And lifts the value to LogicalBlock's state
    handleValueChange = (event) => {
        this.setState(
            {
                value: event.target.value,
            },
            () => {
                // Change logical block state
                const newCond = {
                    name: this.state.name,
                    input: this.state.input,
                    operation: this.state.operation,
                    value: this.state.value,
                };

                this.state.changeAction(newCond);
            }
        );
    };

    render() {
        return (
            <center>
                <div className="block">
                    <form>
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        <label
                                            htmlFor="cb-name"
                                            className="block-label"
                                        >
                                            Name
                                        </label>
                                    </td>
                                    <td>
                                        <input
                                            className="block-input"
                                            type="text"
                                            id="cb-name"
                                            name="cb-name"
                                            value={this.state.name}
                                            onChange={this.handleNameChange}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label
                                            htmlFor="cb-input"
                                            className="block-label"
                                        >
                                            Input
                                        </label>
                                    </td>
                                    <td>
                                        <input
                                            className="block-input"
                                            type="text"
                                            id="cb-input"
                                            name="cb-input"
                                            value={this.state.input}
                                            onChange={this.handleInputChange}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label
                                            htmlFor="cb-operation"
                                            className="block-label"
                                        >
                                            Operation
                                        </label>
                                    </td>
                                    <td>
                                        <input
                                            className="block-input"
                                            type="text"
                                            id="cb-operation"
                                            name="cb-operation"
                                            value={this.state.operation}
                                            onChange={
                                                this.handleOperationChange
                                            }
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label
                                            htmlFor="cb-value"
                                            className="block-label"
                                        >
                                            Value
                                        </label>
                                    </td>
                                    <td>
                                        <input
                                            className="block-input"
                                            type="text"
                                            id="cb-value"
                                            name="cb-value"
                                            value={this.state.value}
                                            onChange={this.handleValueChange}
                                        />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>

                    <button
                        className="block-button"
                        onClick={this.state.deleteAction}
                    >
                        Delete
                    </button>
                </div>
            </center>
        );
    }
}

export default ConditionBlockComponent;

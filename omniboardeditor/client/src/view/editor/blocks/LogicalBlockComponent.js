import React from "react";

import ActionBlockComponent from "./ActionBlockComponent.js";
import ConditionBlockComponent from "./ConditionBlockComponent.js";

// Represents a rule. It can be a "logical_block" (conditions + actions),
// "condition_block" (only conditions), or "action_block" (only actions)
class LogicalBlockComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            idBlock: props.currentBlock.idBlock,
            type: props.currentBlock.type,
            name: props.currentBlock.name,
            conditionsCounter: props.currentBlock.conditionsCounter, // used for key attribute in conditions list
            conditions: props.currentBlock.conditions,
            actionsCounter: props.currentBlock.actionsCounter,
            actions: props.currentBlock.actions,

            onChange: props.onChange,
        };
    }

    // Update the state according to props coming from the editor.
    // Used to lift down editor's state to logical block's state.
    componentWillReceiveProps(newProps) {
        this.setState({
            idBlock: newProps.currentBlock.idBlock,
            type: newProps.currentBlock.type,
            name: newProps.currentBlock.name,
            conditionsCounter: newProps.currentBlock.conditionsCounter,
            conditions: newProps.currentBlock.conditions,
            actionsCounter: newProps.currentBlock.actionsCounter,
            actions: newProps.currentBlock.actions,
        });
    }

    // Change "name" value in state according to the input. And lifts the value to LogicalBlock's state
    handleNameChange = (event) => {
        this.setState({ name: event.target.value }, () =>
            this.state.onChange(this.state)
        );
    };

    // Add a condition to the state. Also add a unique id
    addCondition = (condition) => {
        // Counter
        condition.id = this.state.conditionsCounter;

        // conditionsCounter++
        this.setState(function (previousState, currentProps) {
            return { conditionsCounter: previousState.conditionsCounter + 1 };
        });

        let conditionsArr = Array.from(this.state.conditions);
        conditionsArr.push(condition);

        this.setState({ conditions: conditionsArr }, () =>
            this.state.onChange(this.state)
        );
    };

    // Add an empty condition to the state. Also add a unique id
    addEmptyCondition = () => {
        this.addCondition({
            name: "",
            input: "",
            operation: "",
            value: "",
        });
    };

    // Delete a condition according to its id
    deleteCondition = (id) => {
        let conditionsArr = this.state.conditions.filter(
            (cond) => cond.id !== id
        );

        this.setState({ conditions: conditionsArr }, () =>
            this.state.onChange(this.state)
        );
    };

    // Returns a function to change a condition block with a specific id.
    // Used to apply the change to the editor component
    onConditionChange = (id) => {
        return (newCond) => {
            const index = this.state.conditions.findIndex((c) => c.id === id);

            // Set id in newCond
            newCond.id = this.state.conditions[index].id;

            // Insert newCond
            let condArr = Array.from(this.state.conditions);
            condArr.splice(index, 1, newCond);

            this.setState({ conditions: condArr }, () =>
                this.state.onChange(this.state)
            );
        };
    };

    // Add a condition to the state. Also add a unique id
    addAction = (action) => {
        // Counter
        action.id = this.state.actionsCounter;

        // actionsCounter++
        this.setState(function (previousState, currentProps) {
            return { actionsCounter: previousState.actionsCounter + 1 };
        });

        let actionsArr = Array.from(this.state.actions);
        actionsArr.push(action);

        this.setState({ actions: actionsArr }, () =>
            this.state.onChange(this.state)
        );
    };

    // Add an empty action to the state. Also add a unique id
    addEmptyAction = () => {
        this.addAction({
            name: "",
            action: "",
            output: "",
            value: "",
            param: "",
        });
    };

    // Delete an action according to its id
    deleteAction = (id) => {
        let actionsArr = this.state.actions.filter((act) => act.id !== id);

        this.setState({ actions: actionsArr }, () =>
            this.state.onChange(this.state)
        );
    };

    // Returns a function to change an action block with a specific id.
    // Used to apply the change to the editor component
    onActionChange = (id) => {
        return (newAct) => {
            const index = this.state.actions.findIndex((c) => c.id === id);

            // Set id in newAct
            newAct.id = this.state.actions[index].id;

            // Insert newAct
            let actArr = Array.from(this.state.actions);
            actArr.splice(index, 1, newAct);

            this.setState({ actions: actArr }, () =>
                this.state.onChange(this.state)
            );
        };
    };

    render() {
        // Show or hide conditions/actions components according to the type
        const type = this.state.type;

        let conditions;
        if (type === "logical_block" || type === "conditional_block") {
            conditions = (
                <div>
                    <p className="block-title">Conditions</p>
                    <div className="block-list">
                        {this.state.conditions.map((cond) => (
                            <ConditionBlockComponent
                                key={cond.id}
                                name={cond.name}
                                input={cond.input}
                                operation={cond.operation}
                                value={cond.value}
                                deleteAction={this.deleteCondition.bind(
                                    this,
                                    cond.id
                                )}
                                changeAction={this.onConditionChange(cond.id)}
                            />
                        ))}
                    </div>

                    <button
                        className="plus-button"
                        onClick={this.addEmptyCondition}
                    >
                        +
                    </button>
                </div>
            );
        } else {
            conditions = <div></div>;
        }

        let actions;
        if (type === "logical_block" || type === "action_block") {
            actions = (
                <div>
                    <p className="block-title">Actions</p>
                    <div className="block-list">
                        {this.state.actions.map((act) => (
                            <ActionBlockComponent
                                key={act.id}
                                name={act.name}
                                action={act.action}
                                output={act.output}
                                value={act.value}
                                param={act.param}
                                deleteAction={this.deleteAction.bind(
                                    this,
                                    act.id
                                )}
                                changeAction={this.onActionChange(act.id)}
                            />
                        ))}
                    </div>

                    <button
                        className="plus-button"
                        onClick={this.addEmptyAction}
                    >
                        +
                    </button>
                </div>
            );
        } else {
            actions = <div></div>;
        }

        // Show message if there is no block
        let boards;
        if (!type) {
            boards = (
                <p>
                    No rule has been selected. Create one using "New..."
                    buttons, or you can select one.
                </p>
            );
        } else {
            boards = (
                <div>
                    <p className="block-title">Type : {this.state.type}</p>

                    {/* Used for tests */}
                    <p className="hidden">
                        <label htmlFor="lb_name">Name</label>
                    </p>

                    <input
                        className="input-blockd"
                        placeholder="block name"
                        id="lb_name"
                        name="lb_name"
                        value={this.state.name}
                        onChange={this.handleNameChange}
                    />

                    <div>
                        {conditions}
                        {actions}
                    </div>
                </div>
            );
        }

        return <div className="rule">{boards}</div>;
    }
}

export default LogicalBlockComponent;

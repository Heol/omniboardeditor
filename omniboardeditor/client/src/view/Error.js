import React from "react";
import { Link } from "react-router-dom";

// Error 404 page
function Error() {
    return (
        <div>
            <center>
                <h1>Oups... You got lost !</h1>
                <img
                    src="https://media.tenor.com/images/d61b6fe30deeb262e4a2b8319dfd4c22/tenor.gif"
                    alt=""
                />
                <br />
                <Link to="/">
                    <button>Return to safety</button>
                </Link>
            </center>
        </div>
    );
}

export default Error;

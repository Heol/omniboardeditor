/**
 * This class manages the display of the home page
 */

 import React from "react";
 import {Link} from 'react-router-dom';
 
 /**
  * Allows to display the home page of OMNIBOARD Editor
  */
 class TitleMenu extends React.Component {
 
     render() {
         return (
             <div className="stack-display">
 
                 <h1 className="title-menu"> OMNIBOARD EDITOR</h1>
 
                 <Link to="/new">
                     <button className="button-menu">NEW FILE</button>
                 </Link>
 
                 <Link to="/files">
                     <button className="button-menu">LOAD A FILE</button>
                 </Link>
 
                 <Link to="/flippers">
                     <button className="button-menu">PINBALLS</button>
                 </Link>
             </div>
         );
     }
 }
 
 export default TitleMenu;
 
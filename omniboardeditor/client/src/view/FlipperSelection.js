import React from "react";
import { Link } from "react-router-dom";

function Flipper({ name, link, delFlip }) {
    return (
        <div key={Math.random() * 9999999999999}>
            <p className="fourth-title">{name}</p>
            <img alt={name} className="img-flipper" src={link} width="100" />
            <button
                className="delete-flipper"
                onClick={() => {
                    delFlip(name);
                }}
            >
                Delete
            </button>
        </div>
    );
}

// This component allows you to select a pinball.
class FlipperSelection extends React.Component {
    constructor(props) {
        super(props);
        this.loadFlipper();
    }

    state = {
        flippers: [],
        name: "",
        link: "",
    };

    loadFlipper = () => {
        // Takes pinballs in the database.
        var init = { method: "GET" };
        fetch("/flipper/getflippers", init)
            .then((res) => {
                return res.json();
            })
            .then((result) => {
                this.setState({ flippers: result });
                console.log(result);
            });
    };

    handleNameChange = (event) => {
        this.setState({ name: event.target.value });
    };

    handleLinkChange = (event) => {
        this.setState({ link: event.target.value });
    };

    deleteFlipper = (name) => {
        var init = { method: "POST" };
        fetch("/flipper/delete?name=" + name, init);
        this.loadFlipper();
    };

    addFlipper = () => {
        var init = { method: "POST" };
        fetch(
            "/flipper/add?name=" + this.state.name + "&link=" + this.state.link,
            init
        );
        this.loadFlipper();
    };

    render() {
        const rows = [];

        this.state.flippers.forEach((flipper) => {
            rows.push(
                <Flipper
                    name={flipper.name}
                    link={flipper.link}
                    delFlip={this.deleteFlipper}
                />
            );
        });

        return (
            <center>
                <h1 className="title-menu"> MY PINBALLS </h1>

                <h2 className="second-title">ADD A PINBALL</h2>
                <label className="label-flipper">
                    Name :
                    <input
                        className="input-flipper"
                        type="text"
                        onChange={this.handleNameChange}
                    />
                </label>
                <label className="label-flipper">
                    Link :
                    <input
                        className="input-flipper"
                        type="text"
                        onChange={this.handleLinkChange}
                    />
                </label>
                <button
                    className="send-button"
                    type="submit"
                    value="add"
                    onClick={this.addFlipper}
                >
                    Send
                </button>

                <h2 className="second-title">MY LIST</h2>

                <table className="list">
                    <tbody>{rows}</tbody>
                </table>

                <Link to="/">
                    <button className="exit-button">Exit</button>
                </Link>
            </center>
        );
    }
}

export default FlipperSelection;

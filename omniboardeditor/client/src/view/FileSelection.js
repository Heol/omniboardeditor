import React from "react";
import {Link} from "react-router-dom";

function File({id, name, flipper}) {

    var url = "/editor/"+id;

    var deleteHandle = () => {
        var init = { method: "POST"};
        fetch("/file/delete?id="+id, init);
    }

    return (
        <div >
                <Link to={url}>
                    <button className="list-flippers-fileSelection">{name} on {flipper}</button>
                </Link>
                <Link to="/files">
                    <button className="delete-file" onClick={deleteHandle}>DELETE</button>
                </Link>
        </div>
    )
}

// This component allows you to select a file. 
class FileSelection extends React.Component {

    state = {
        files: []
    }

    constructor(props) {
        super(props);
        this.loadFiles();
        
    }

    loadFiles() {
        // Takes files in the database.
        var init = { method: "GET" };
        fetch("/file/getfilesheader", init)
        .then((res) => {
            return res.json();
        })
        .then(
            (result) => {
                this.setState({files: result});
            }
        );
    }

    render() {

        this.loadFiles();

        const rows = [];

        this.state.files.forEach(file => {
            rows.push(<File id={file.id} name={file.name} flipper={file.flipper}/>)
        });

        return (
            <div>
                <h1 className="title-menu"> CHOOSE A FILE </h1>

                <table className="divFile">
                    <tbody>{rows}</tbody>
                </table>

                <Link to="/">
                    <button className = "exit-button-file"> EXIT </button>
                </Link>
            </div>
        )
    }
}

export default FileSelection;
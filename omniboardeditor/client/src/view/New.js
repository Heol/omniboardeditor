import React from "react";
import {Link} from "react-router-dom";

function Flipper({name, link, onSelect}) {
    return (
        <div>
            <center>
                <h3 className="third-title">{name}</h3>
                <img src={link} width="150px" alt="pinball"/>
                <br/>
                <button className="button-select" onClick={() => {onSelect(name)}}>Select</button>
            </center>
        </div>
    )
}

// This component allows you to create a file with a name and a pinball. 
class New extends React.Component {

    state = {
        id: Math.floor((Math.random()*999999999)),
        name: "",
        flippers: [],
        selectedFlipper: ""
    }

    constructor(props) {
        super(props);

        // Takes pinballs in the database.
        var init = { method: "GET" };
        fetch("/flipper/getflippers", init)
        .then((res) => {
            return res.json();
        })
        .then(
            (result) => {
                this.setState({flippers: result});
            }
        );
    }

    handleNameChange = (event) => {
        this.setState({ name: event.target.value });
    }

    handleFlipperChange = (flipper) => {
        this.setState({ selectedFlipper: flipper });
    }

    createFile = () => {

        var init = {method: "POST"};
        fetch("/file/save?id=" + this.state.id + "&name=" + this.state.name + "&flipper=" + this.state.selectedFlipper, init)
    }

    render() {

        var url = "/editor/" + this.state.id;

        const rows = [];

        this.state.flippers.forEach(flipper => {
            rows.push(<Flipper name={flipper.name} link={flipper.link} onSelect={this.handleFlipperChange} />);
        });

        return (
            <center>
                <h1 className="title-menu"> CHOOSE A NAME AND A PINBALL </h1>
                <input placeholder="Enter a file name" className ="input-name-flipper" onChange={this.handleNameChange}></input>

                <h2 className="second-title">PLEASE SELECT A PINBALL :</h2>
                
                {rows}

                <Link to={url}>
                    <button className = "exit-button" onClick={this.createFile}>
                        CREATE
                    </button>
                </Link>

                <Link to="/">
                    <button className = "exit-button">
                        EXIT
                    </button>
                </Link>
            </center>
        )
    }
}

export default New;
import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import TitleMenu from "./view/TitleMenu.js";
import EditorComponent from './view/editor/EditorComponent.js';
import FlipperSelection from './view/FlipperSelection.js';
import FileSelection from './view/FileSelection.js';
import New from './view/New.js';
import Error from './view/Error.js';

class App extends React.Component {

    render() {
        return (
            <div>
                <Router>
                    <Switch>
                        <Route path='/' exact component={TitleMenu}/>
                        <Route path='/new' exact component={New}/>
                        <Route path='/editor' exact component={EditorComponent}/>
                        <Route path='/editor/:id' exact component={EditorComponent}/>
                        <Route path='/files' exact component={FileSelection}/>
                        <Route path='/flippers' exact component={FlipperSelection}/>
                        <Route component={Error}/>
                    </Switch>
                </Router>
            </div>
        );
    }
}

export default App;

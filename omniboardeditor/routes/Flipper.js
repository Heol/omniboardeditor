var express = require("express");
var router = express.Router();
var bodyParser = require("body-parser");

var jsonParser = bodyParser.json();

const FlipperDAO = require("../src/FlipperDAO.js");

// this route gives you every flippers.
router.get("/getflippers", function (req, res, next) {
    var dao = new FlipperDAO();
    dao.getFlippers((flippers) => {
        res.send(flippers);
    });
});

// this route gives you one flipper in particulary.
// It takes the name of the wanted flipper in a query param "flippername".
router.get("/getflipper", function (req, res, next) {
    if (req.query.flippername !== undefined) {
        var dao = new FlipperDAO();
        dao.getFLipper(req.query.flippername, (flipper) => {
            res.send(flipper);
        });
    }
});

// This route allows you to add a flipper.
// It takes the name and the link of the new flipper in query param "name" and "link".
router.post("/add", function (req, res, next) {
    if (req.query.name !== undefined && req.query.link !== undefined) {
        var dao = new FlipperDAO();

        var flipper = {
            name: req.query.name,
            link: req.query.link
        }

        dao.addFlipper(flipper);
        res.send("Flipper : " + req.query.name + " added");
    }
    else res.send("No flipper in post request");
});

// This route allows you to modify a flipper (only the link).
// It takes the name and the new link of the flipper in query param "name" and "link".
router.post("/update", jsonParser, function (req, res, next) {
    if (req.query.flipper !== undefined) {
        var dao = new FlipperDAO();
        dao.updateFlipper(req.query.name, req.body.link);
        res.send("Flipper : " + req.query.name + " updated");
    } else res.send("No flipper in post request");
});

// this route allows you to delete a flipper.
// It takes the name of the flipper to delete in a query param "name".
router.post("/delete", jsonParser, function (req, res, next) {
    if (req.query.name !== undefined) {
        var dao = new FlipperDAO();
        dao.deleteFlipper(req.query.name);
        res.send("Flipper : " + req.query.name + " deleted");
    } else res.send("No flipper's name in post request");
});

module.exports = router;

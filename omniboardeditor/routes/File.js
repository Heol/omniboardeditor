var express = require("express");
var router = express.Router();
var bodyParser = require("body-parser");

var jsonParser = bodyParser.json();

const RulesFilesDAO = require("../src/RulesFilesDAO.js");

// This route gives you every files.
router.get("/getfiles", function (req, res, next) {
    var dao = new RulesFilesDAO();
    dao.getRulesFiles((files) => {
        res.send(files);
    });
});

// This routes gives you every files but only their id, name, and flipper. 
router.get('/getfilesheader', function(req, res, next) {

    var dao = new RulesFilesDAO();
    dao.getRulesFiles((files) => {
        var response = [];
        files.forEach((item, i) => {
            var itemHeader = {
                id: item.id,
                name: item.name,
                flipper: item.flipper
            }
            response.push(itemHeader);
        });
        res.send(response);
    });
});

// This route gives you a file in particulary.
// It takes the id of the wanted file in a query param "id".
router.get('/getfile', function(req, res, next) {

    if (req.query.id !== undefined) {
        var dao = new RulesFilesDAO();
        dao.getRulesFile(req.query.id,(file) => {
            res.send(file);
        });
    }
});

// This routes allows you to save a file passed in the query body in "file".
// It knows if the file exist in database with body data and query params.
// If the file doesn't exist in the database, it will be add.
// If the file already exist in the database, it will be update.
router.post('/save', jsonParser, function(req, res, next) {
    if (req.body.file !== undefined) {
        var dao = new RulesFilesDAO();

        if (req.body.file.id !== undefined && req.body.file.id !== "") {
            dao.getRulesFile(req.body.file.id, (file) => {
                if (file !== undefined) {
                    dao.updateRulesFile(req.body.file);
                }
                else {
                    dao.addRulesFile(req.body.file);
                }
                res.send(req.body.file.id);
            });
        }
    }
    else if (req.query.id !== undefined && req.query.name !== undefined && req.query.flipper !== undefined) {

        var dao = new RulesFilesDAO();

        var file = {
            id: req.query.id,
            name: req.query.name,
            flipper: req.query.flipper,
            rules: []
        }

        dao.addRulesFile(file);
        res.send("File " + req.query.id + " " + req.query.name + " created");
    }
    else {
        res.send('No file in post request');
    }
});

// This route allows you to delete a file by giving its id in query param.
router.post('/delete', function(req, res, next) {
    if (req.query.id !== undefined) {
        var dao = new RulesFilesDAO();
        dao.deleteRulesFile(req.query.id);
        res.send("RulesFile " + req.query.id + " deleted");
    }
})

module.exports = router;

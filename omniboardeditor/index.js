const express = require("express");
var session = require("express-session");
const PORT = 5000;
var bodyParser = require("body-parser");

var fileRouter = require("./routes/File.js");
var flipperRouter = require("./routes/Flipper.js");

const app = express();

// CORS policy
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// Routes
app.use("/file", fileRouter);
app.use("/flipper", flipperRouter);

app.use(bodyParser.urlencoded({ extended: true }));

// parse application/json
app.use(bodyParser.json());

app.use(function (req, res) {
    res.setHeader("Content-Type", "text/plain");
    res.write("you posted:\n");
    res.end(JSON.stringify(req.body, null, 2));
});

app.use(
    session({
        secret: "keyboard cat",
        resave: false,
        saveUninitialized: true,
        cookie: { secure: true },
    })
);

app.listen(PORT, () => {
    console.log("Listening on " + PORT);
});

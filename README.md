<h1 style="font-size: 3em;">Groupe RADA</h1>

[Guide installation et lancement](#installation-et-lancement-de-la-version-de-d%C3%A9veloppement)

# Contexte du projet

M. Roirand est un passionné de flippers. Seulement, il n’est pas satisfait de la fiabilité des cartes électroniques qui contiennent toute la partie logique du jeu.
De ce fait, le client nous a soumis le projet OmniBoardEditor, dont le principe est de réussir à remplacer physiquement un ensemble de cartes électroniques qui pilotent un flipper par une seule et unique carte électronique moderne, elle-même pilotée par un Raspberry PI. L’objectif est donc de créer un éditeur d’algorithme permettant de modéliser un flipper et son fonctionnement pour un utilisateur. Cet éditeur devra prendre en compte l’ensemble de la partie logicielle sur Raspberry, c’est-à-dire comptabiliser les points, prendre en compte les lumières et les décisions de jeu, tout ça de manière customisable.


# Présentation de l'équipe RADA

Je suis Allan BERNIER, originaire de Rennes et étudiant en deuxième année d'informatique à l'IUT de vannes. Je suis passionnée par l'informatique depuis toujours et je réalise beaucoup de projets personnels. Dans mon avenir professionnel j'aimerais devenir développeur informatique dans un premier temps puis ensuite chef de projet. Je suis donc le chef de ce projet ce qui consiste à être l'interlocuteur privilégié du groupe avec l'extérieur (client, maître d'œuvre), répartir le travail dans le groupe, animer celui-ci, fixer les rendez-vous avec le client et le maître d'œuvre définir les échéances du groupe, réaliser les livraisons auprès du client et du maître d'œuvre et signaler les difficultés du groupe.

Je m'appelle Donovan BRUN, j'ai 18 ans et je suis actuellement en deuxième année de DUT Informatique à Vannes. Titulaire d'un bac STI2D, j'aimerais dans mon avenir professionnel travailler dans l'informatique et plus précisément de l'ingénierie logicielle. Durant ce projet je serais le responsable de la communication ce qui consiste au maintien du site web du groupe projet (rapports, comptes rendus, logiciel, support de soutenance) ainsi qu’à la rédaction des comptes-rendus des réunions.

Je suis Romain DINEL, un étudiant de 18 ans en seconde année à l’IUT de Vannes, originaire de Rennes et diplômé d’un BAC S. Passionné du monde de l'informatique mais avec très peu d'expériences, j'ai décidé d'entreprendre un IUT informatique principalement pour apprendre les bases de la programmation. Je serai responsable de la documentation de ce projet. Je veillerai à ce que les documents respectent les formats et qu'ils soient livrés dans les temps.

Je m'appelle Antoine BRAUD, j'ai 19 ans et je suis étudiant en seconde année d'informatique à l'IUT de Vannes. Je serai, durant ce projet, chargé des tests de l'application. Je dois effectuer différentes phases de tests (unitaires, intégration, performance, etc…) pour m'assurer que l'application fonctionne correctement et selon les exigences du client en faisant des rapports sur ces tests.

Je m'appelle Yoann DEWILDE, j'ai 19 ans, et je suis actuellement en deuxième année de DUT Informatique à Vannes. Je suis passionné par la programmation, et notamment la développement d'interfaces permettant de créer des choses ; c'est pour cela que j'ai rejoint ce projet.
Mon rôle sera de gérer les différents documents qui concernent les spécifications, et vérifier qu'ils sont conformes.


# Mise en oeuvre du projet

L'objectif principal du projet est de fournir au client un éditeur sous forme d'une interface web lui permettant de créer des règles pour un flipper.
L'éditeur devra à partir d'input (couloir, trous, cibles, contracteur de tilt, score, etc…) créer des actions (éclairage, son, etc…) et gérer des états (score, etc…). La combinaison de ceci formera une règle. L'ensemble de règles est ensuite traduit et stocké dans un fichier (voir annexe). L'interface web permettra de charger un fichier existant, le sauvegarder ou d'en commencer un nouveau. Un fichier de règle correspond à un flipper, mais un flipper peut avoir plusieurs fichiers de règles.

L'éditeur est décomposé en trois sections : 
- Une image représentant flipper avec la possibilité de survoler avec la souris certaines zones afin d'afficher les règles en lien avec ces zones et le type de zone survolée (type d'input, d'action ou d'état). Un clic sur un élément du flipper redirigera 
- L'éditeur graphique permettant de créer ces règles facilement sous forme de blocs WYSIWYG (What You See Is What You Get).
- Une section à deux onglets, l'un permettant de voir l'arborescence de l'algorithme et l'autre permettant d'afficher le fichier de règles en cours de modification.

L'éditeur devra être le plus libre possible permettant aux utilisateurs de créer leurs nouvelles règles sans être limité. Par exemple permettre une customisation maximal pour créer des règles en fonction du joueur. Les règles devront être dynamiques, c'est à dire qu'elles peuvent être désactivée ou activée en fonction des états du jeu. Les règles auront un contexte (début de partie, en cours de partie, etc…), une action déclencheur ainsi que des actions qu'elle devra réalisées. Les règles sont bien entendu ajoutable, supprimable et modifiable et il n'y a pas de limite de règle, l'utilisateur pourra en faire autant qu'il le souhaite. L'interface devra être en anglais. L’éditeur sera livré avec deux exemples de fichier de règles correspondant à deux flippers qu’aura choisit M. Roirand.


# Installation et lancement de la version de développement

Le projet est composé de deux serveurs, un serveur ExpressJS pour le backend et un serveur ReactJS pour le frontend. Il y a aussi le serveur MongoDB pour la base de données. Les deux serveurs NodeJS tournent sur la même adresse avec des ports différents.

## Lancement du backend

Placez vous dans le répertoire omniboardeditor (où il y a un package.json) et exécutez les commandes suivantes :  
`npm install`  
`npm start`  

## Lancement du frontend

Placez vous dans le répertoire omniboardeditor/client et exécutez les (mêmes) commandes suivantes :  
`npm install`  
`npm start`  

## Accès à l'application web

Ouvrez votre navigateur favori et allez à cette adresse : [ici](http://localhost:3000)  
Vous voilà prêt à utiliser OmniBoardEditor !
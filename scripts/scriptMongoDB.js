// connexion à la bdd
conn = new Mongo();
db = conn.getDB("OmniBoardEditor");

// création des collections
db.createCollection("rulesFiles");
db.createCollection("flippers");

var file = {
	"id":"1",
	"name":"rulesFile1",
	"flipper":"pinball1",
	"rules": [
		{
			"type":"logical_block",
			"name":"2nd_bonus",
			"conditions":[{"name":"score greater than 200","input":"score","operation":"greater","value":"200"},{"name":"lane a is down","input":"lane_a","operation":"state","value":"down"},{"name":"lane b is down","input":"lane_b","operation":"state","value":"down"},{"name":"lane c is down","input":"lane_c","operation":"state","value":"down"}],
			"actions":[{"name":"play bonus sound","action":"play","value":"bonus.mp3"},{"name":"increase score by 1000","action":"increase","output":"score","value":1000},{"name":"flash right lamps","action":"flash_lamps_right"},{"name":"flash left lamps","action":"flash_lamps_left"}]
		},
		{
			"type":"conditional_block",
			"name":"lanes_a_b_c_down",
			"conditions":[{"name":"lane a is down","input":"lane_a","operation":"state","value":"down"},{"name":"lane b is down","input":"lane_b","operation":"state","value":"down"},{"name":"lane c is down","input":"lane_c","operation":"state","value":"down"}]
		},
		{
			"type":"action_block",
			"name":"flash_lamps_right",
			"actions":[{"name":"flash_lamp_1","action":"flash","output":"lamp_1","param":300},{"name":"flash_lamp_2","action":"flash","output":"lamp_2","param":300},{"name":"flash_lamp_3","action":"flash","output":"lamp_3","param":300}]
		}
	]
}

var file2 = {
    "id": "2",
    "name": "rulesFile2",
    "flipper": "pinball2",
    "rules": [{
        "type": "logical_block",
        "name": "2nd_bonus",
        "conditions": [{
            "name": "score greater than 2 000",
            "input": "score",
            "operation": "greater",
            "value": 2000
        }, {
            "name": "lane a is down",
            "input": "lane_a",
            "operation": "state",
            "value": "down"
        }, {
            "name": "lane b is down",
            "input": "lane_b",
            "operation": "state",
            "value": "down"
        }, {
            "name": "lane c is down",
            "input": "lane_c",
            "operation": "state",
            "value": "down"
        }],
        "actions": [{
            "name": "play bonus sound",
            "action": "play",
            "value": "bonus.mp3"
        }, {
            "name": "increase score by 1000",
            "action": "increase",
            "output": "score",
            "value": 1000
        }, {
            "name": "flash right lamps",
            "action": "flash_lamps_right"
        }, {
            "name": "flash left lamps",
            "action": "flash_lamps_left"
        }]
    }, {
        "type": "conditional_block",
        "name": "lanes_a_b_c_down",
        "conditions": [{
            "name": "lane a is down",
            "input": "lane_a",
            "operation": "state",
            "value": "down"
        }, {
            "name": "lane b is down",
            "input": "lane_b",
            "operation": "state",
            "value": "down"
        }, {
            "name": "lane c is down",
            "input": "lane_c",
            "operation": "state",
            "value": "down"
        }]
    }, {
        "type": "action_block",
        "name": "flash_lamps_right",
        "actions": [{
            "name": "flash_lamp_1",
            "action": "flash",
            "output": "lamp_1",
            "param": 300
        }, {
            "name": "flash_lamp_2",
            "action": "flash",
            "output": "lamp_2",
            "param": 300
        }, {
            "name": "flash_lamp_3",
            "action": "flash",
            "output": "lamp_3",
            "param": 300
        }]
    }]
}

db.rulesFiles.insertOne(file);
db.rulesFiles.insertOne(file2);

var flipper = {
	"name":"pinball1",
	"link":"https://www.lyon-flipper.com/media/cache/produit_images_big/uploads/media/5abb83cebf491-flipper-bailly-gilligans-island-14.jpg"
};

var flipper2 = {
    "name": "pinball2",
    "link": "https://www.lamaisondubillard.com/3399-medium_default/flipper-en-bois-flipper-espace.jpg"
}

db.flippers.insertOne(flipper);
db.flippers.insertOne(flipper2);